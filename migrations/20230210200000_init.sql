-- +goose Up 
-- +goose NO TRANSACTION
CREATE TABLE IF NOT EXISTS geo_objects(
    id BIGSERIAL PRIMARY KEY,
    lon NUMERIC(8,6),
    lat NUMERIC(8,6),
    title TEXT,
    content TEXT
    );
-- +goose Down 
-- +goose NO TRANSACTION
DROP TABLE geo_objects;
