-- +goose Up 
-- +goose NO TRANSACTION
CREATE UNIQUE INDEX geo_objects_coordinates ON geo_objects (lon, lat);
-- +goose Down 
-- +goose NO TRANSACTION
DROP INDEX geo_objects_coordinates;
