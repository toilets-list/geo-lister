package sql

import geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"

func geoObjectFromDBO(dbo GeoObjectDBO) geoobjects.GeoObject {
	return geoobjects.GeoObject{
		ID: dbo.ID,
		Coordinates: geoobjects.Coordinates{
			Lon: dbo.Lon,
			Lat: dbo.Lat,
		},
		Title:   dbo.Title,
		Content: dbo.Content,
	}
}
