package sql

import (
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/pgxscan"
	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
)

type GeoObjectDBO struct {
	ID      int64
	Lon     float64
	Lat     float64
	Title   string
	Content string
}

func (s *Storage) GetGeoObjectByID(ctx context.Context, objectID int64) (geoobjects.GeoObject, error) {
	builder := squirrel.Select(
		"id",
		"lon",
		"lat",
		"title",
		"content",
	).From(
		"geo_objects",
	).Where(
		squirrel.Eq{"id": objectID},
	)
	query, args, err := builder.PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return geoobjects.GeoObject{}, err
	}

	dbo := GeoObjectDBO{}
	err = pgxscan.Get(ctx, s.db, &dbo, query, args...)
	if err != nil {
		return geoobjects.GeoObject{}, err
	}

	return geoObjectFromDBO(dbo), err
}

func (s *Storage) ListGeoObjectsInArea(
	ctx context.Context,
	filter geoobjects.AreaFilter,
) ([]geoobjects.GeoObject, error) {
	builder := squirrel.Select(
		"id",
		"lon",
		"lat",
		"title",
		"content",
	).From(
		"geo_objects",
	).Where(
		squirrel.GtOrEq{"lon": filter.MinLon},
		squirrel.LtOrEq{"lon": filter.MaxLon},
		squirrel.GtOrEq{"lat": filter.MinLat},
		squirrel.LtOrEq{"lat": filter.MaxLat},
	)
	query, args, err := builder.PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	row, err := s.db.Query(ctx, query, args...)
	list := []geoobjects.GeoObject{}
	dbo := GeoObjectDBO{}
	if row.Next() {
		err = pgxscan.ScanRow(&dbo, row)
		if err != nil {
			return nil, err
		}

		list = append(list, geoObjectFromDBO(dbo))
	}

	return list, err
}

func (s *Storage) DeleteGeoObject(ctx context.Context, objectID int64) error {
	query, args, err := squirrel.Delete(
		"geo_objects",
	).Where(
		squirrel.Eq{"id": objectID},
	).PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return err
	}

	_, err = s.db.Exec(ctx, query, args...)

	return err
}

func (s *Storage) CreateGeoObject(ctx context.Context, object geoobjects.GeoObject) (int64, error) {
	builder := squirrel.Insert(
		"geo_objects",
	).SetMap(map[string]interface{}{
		"lon":     object.Coordinates.Lon,
		"lat":     object.Coordinates.Lat,
		"title":   object.Title,
		"content": object.Content,
	}).Suffix("RETURNING id")
	query, args, err := builder.PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return 0, err
	}

	var objectID int64

	row := s.db.QueryRow(ctx, query, args...)
	if err := row.Scan(&objectID); err != nil {
		return 0, err
	}

	return objectID, nil
}

func (s *Storage) UpdateGeoObject(ctx context.Context, object geoobjects.GeoObject) error {
	builder := squirrel.Update(
		"geo_objects",
	).SetMap(map[string]interface{}{
		"lon":     object.Coordinates.Lon,
		"lat":     object.Coordinates.Lat,
		"title":   object.Title,
		"content": object.Content,
	}).Where(
		squirrel.Eq{"id": object.ID},
	)
	query, args, err := builder.PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return err
	}

	_, err = s.db.Exec(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}
