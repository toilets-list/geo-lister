package sql

import (
	"context"
	"fmt"
	"net"
	"os"

	pgx "github.com/jackc/pgx/v4"
)

type Storage struct {
	db *pgx.Conn
}

func NewStorage(ctx context.Context, connstr string) (*Storage, error) {
	conn, err := pgx.Connect(ctx, connstr)
	if err != nil {
		return nil, err
	}

	err = conn.Ping(ctx)
	if err != nil {
		return nil, err
	}

	return &Storage{
		db: conn,
	}, nil
}

func ConnectionStr() string {
	user := os.Getenv("DB_USERNAME")
	password := os.Getenv("DB_PASSWORD")
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	base := os.Getenv("DB_BASE")

	return fmt.Sprintf("postgresql://%s/%s?user=%s&password=%s", net.JoinHostPort(host, port), base, user, password)
}
