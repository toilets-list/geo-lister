package geoobjects

import "context"

type DeleteGeoObjectHandler interface {
	Handle(context.Context, int64) error
}

type deleteGeoObjectHandler struct {
	geoObjectDeleter GeoObjectDeleter
}

func NewDeleteGeoObjectHandler(
	geoObjectDeleter GeoObjectDeleter,
) DeleteGeoObjectHandler {
	return &deleteGeoObjectHandler{
		geoObjectDeleter: geoObjectDeleter,
	}
}

func (h *deleteGeoObjectHandler) Handle(ctx context.Context, id int64) error {
	return h.geoObjectDeleter.DeleteGeoObject(ctx, id)
}
