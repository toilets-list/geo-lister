package geoobjects

type GeoObject struct {
	ID          int64
	Coordinates Coordinates
	Title       string
	Content     string
}

type Coordinates struct {
	Lon float64
	Lat float64
}

type AreaFilter struct {
	MinLon float64
	MaxLon float64
	MinLat float64
	MaxLat float64
}
