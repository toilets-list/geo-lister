package geoobjects

import "context"

type GetGeoObjectHandler interface {
	Handle(context.Context, int64) (GeoObject, error)
}

type getGeoObjectHandler struct {
	storage GeoObjectByIDGetter
}

func NewGetGeoObjectHandler(storage GeoObjectByIDGetter) GetGeoObjectHandler {
	return &getGeoObjectHandler{
		storage: storage,
	}
}

func (h getGeoObjectHandler) Handle(ctx context.Context, id int64) (GeoObject, error) {
	objectDBO, err := h.storage.GetGeoObjectByID(ctx, id)

	return objectDBO, err
}
