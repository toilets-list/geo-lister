package geoobjects

import (
	"context"
)

type UpdateGeoObjectHandler interface {
	Handle(context.Context, GeoObject) error
}

type updateGeoObjectHandler struct {
	geoObjectUpdater GeoObjectUpdater
}

func NewUpdateGeoObjectHandler(
	geoObjectUpdater GeoObjectUpdater,
) UpdateGeoObjectHandler {
	return &updateGeoObjectHandler{
		geoObjectUpdater: geoObjectUpdater,
	}
}

func (h *updateGeoObjectHandler) Handle(ctx context.Context, object GeoObject) error {
	return h.geoObjectUpdater.UpdateGeoObject(ctx, object)
}
