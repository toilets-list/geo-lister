package geoobjects

import "context"

type CreateGeoObjectHandler interface {
	Handle(context.Context, GeoObject) (int64, error)
}

type createGeoObjectHandler struct {
	storage GeoObjectCreator
}

func NewCreateGeoObjectHandler(storage GeoObjectCreator) CreateGeoObjectHandler {
	return &createGeoObjectHandler{
		storage: storage,
	}
}

func (h createGeoObjectHandler) Handle(ctx context.Context, object GeoObject) (int64, error) {
	return h.storage.CreateGeoObject(ctx, object)
}
