package geoobjects

import (
	"context"
	"errors"
)

var ErrEmptyArea = errors.New("empty area")

type ListGeoObjectInAreaHandler interface {
	Handle(context.Context, []Coordinates) ([]GeoObject, error)
}

type ListGeoObjectInAreaFilter struct{}

type listGeoObjectInAreaHandler struct {
	storage ListGeoObjectsInArea
}

func NewListGeoObjectInAreaHandler(storage ListGeoObjectsInArea) ListGeoObjectInAreaHandler {
	return &listGeoObjectInAreaHandler{
		storage: storage,
	}
}

func (h listGeoObjectInAreaHandler) Handle(ctx context.Context, dots []Coordinates) ([]GeoObject, error) {
	if len(dots) == 0 {
		return nil, ErrEmptyArea
	}

	filter := AreaFilter{
		MinLon: dots[0].Lon,
		MaxLon: dots[0].Lon,
		MinLat: dots[0].Lat,
		MaxLat: dots[0].Lat,
	}
	for _, dot := range dots {
		if dot.Lat > filter.MaxLat {
			filter.MaxLat = dot.Lat
		}
		if dot.Lat < filter.MinLat {
			filter.MinLat = dot.Lat
		}
		if dot.Lon > filter.MaxLon {
			filter.MaxLon = dot.Lon
		}
		if dot.Lon < filter.MinLon {
			filter.MinLon = dot.Lon
		}
	}

	objectsDBO, err := h.storage.ListGeoObjectsInArea(ctx, filter)

	return objectsDBO, err
}
