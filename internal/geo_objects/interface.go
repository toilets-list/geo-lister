package geoobjects

import (
	"context"
)

//go:generate mockgen -destination interfaces_mock.go -package schedules . GeoObjectSaver,GeoObjectDeleter,ListGeoObjectsInArea,GeoObjectByIDGetter

type GeoObjectByIDGetter interface {
	GetGeoObjectByID(ctx context.Context, ID int64) (GeoObject, error)
}

type ListGeoObjectsInArea interface {
	ListGeoObjectsInArea(ctx context.Context, filter AreaFilter) ([]GeoObject, error)
}

type GeoObjectDeleter interface {
	DeleteGeoObject(ctx context.Context, ID int64) error
}

type GeoObjectUpdater interface {
	UpdateGeoObject(ctx context.Context, object GeoObject) error
}

type GeoObjectCreator interface {
	CreateGeoObject(ctx context.Context, object GeoObject) (int64, error)
}
