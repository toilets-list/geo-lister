package server

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func (i *geoService) DeleteGeoObject(ctx context.Context, req *desc.DeleteGeoObjectRequest) (*empty.Empty, error) {
	err := i.deleteGeoObjectHandler.Handle(ctx, req.GetId())

	return &empty.Empty{}, statusOf(err).Err()
}
