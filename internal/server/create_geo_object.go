package server

import (
	"context"

	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func (i *geoService) CreateGeoObject(
	ctx context.Context,
	req *desc.CreateGeoObjectRequest,
) (*desc.CreateGeoObjectResponse, error) {
	objectID, err := i.createGeoObjectHandler.Handle(ctx, geoObjectFromProto(req.GetGeoObject()))
	if err != nil {
		return nil, statusOf(err).Err()
	}

	return &desc.CreateGeoObjectResponse{
		Id: objectID,
	}, nil
}
