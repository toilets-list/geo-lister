package server

import (
	"context"

	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func (i *geoService) GetGeoObject(
	ctx context.Context,
	req *desc.GetGeoObjectRequest,
) (*desc.GetGeoObjectResponse, error) {
	obj, err := i.getGeoObjectHandler.Handle(ctx, req.GetId())
	if err != nil {
		return nil, statusOf(err).Err()
	}

	return &desc.GetGeoObjectResponse{
		GeoObject: geoObjectToProto(obj),
	}, nil
}
