package server

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func (i *geoService) UpdateGeoObject(ctx context.Context, req *desc.UpdateGeoObjectRequest) (*empty.Empty, error) {
	err := i.updateGeoObjectHandler.Handle(
		ctx,
		geoObjectFromProto(req.GetGeoObject()),
	)
	if err != nil {
		return nil, statusOf(err).Err()
	}

	return &empty.Empty{}, nil
}
