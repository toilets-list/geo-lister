package server

import (
	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func geoObjectFromProto(proto *desc.GeoObject) geoobjects.GeoObject {
	return geoobjects.GeoObject{
		ID: proto.Id,
		Coordinates: geoobjects.Coordinates{
			Lon: proto.Coordinates.Lon,
			Lat: proto.Coordinates.Lat,
		},
		Title:   proto.Title,
		Content: proto.Content,
	}
}

func geoObjectToProto(object geoobjects.GeoObject) *desc.GeoObject {
	return &desc.GeoObject{
		Id: object.ID,
		Coordinates: &desc.Coordinates{
			Lat: object.Coordinates.Lat,
			Lon: object.Coordinates.Lon,
		},
		Title:   object.Title,
		Content: object.Content,
	}
}
