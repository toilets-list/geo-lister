package server

import (
	"context"

	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
	desc "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

func (i *geoService) ListGeoObjectsInArea(
	ctx context.Context,
	req *desc.ListGeoObjectsInAreaRequest,
) (*desc.ListGeoObjectsInAreaResponse, error) {
	coords := make([]geoobjects.Coordinates, len(req.GetArea()))
	for i, inCoord := range req.GetArea() {
		coords[i] = geoobjects.Coordinates{
			Lon: inCoord.Lon,
			Lat: inCoord.Lat,
		}
	}

	objects, err := i.listGeoObjectInAreaHandler.Handle(ctx, coords)
	if err != nil {
		return nil, statusOf(err).Err()
	}

	res := make([]*desc.GeoObject, len(objects))
	for i, object := range objects {
		res[i] = geoObjectToProto(object)
	}

	return &desc.ListGeoObjectsInAreaResponse{
		GeoObjects: res,
	}, nil
}
