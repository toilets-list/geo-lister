package server

import (
	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
	geo_lister "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
)

type geoService struct {
	getGeoObjectHandler        geoobjects.GetGeoObjectHandler
	createGeoObjectHandler     geoobjects.CreateGeoObjectHandler
	listGeoObjectInAreaHandler geoobjects.ListGeoObjectInAreaHandler
	deleteGeoObjectHandler     geoobjects.DeleteGeoObjectHandler
	updateGeoObjectHandler     geoobjects.UpdateGeoObjectHandler
}

func NewGeoService(
	getGeoObjectHandler geoobjects.GetGeoObjectHandler,
	createGeoObjectHandler geoobjects.CreateGeoObjectHandler,
	listGeoObjectInAreaHandler geoobjects.ListGeoObjectInAreaHandler,
	deleteGeoObjectHandler geoobjects.DeleteGeoObjectHandler,
	updateGeoObjectHandler geoobjects.UpdateGeoObjectHandler,
) geo_lister.GeoServiceServer {
	return &geoService{
		getGeoObjectHandler:        getGeoObjectHandler,
		createGeoObjectHandler:     createGeoObjectHandler,
		listGeoObjectInAreaHandler: listGeoObjectInAreaHandler,
		deleteGeoObjectHandler:     deleteGeoObjectHandler,
		updateGeoObjectHandler:     updateGeoObjectHandler,
	}
}
