package server

import (
	"errors"

	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func statusOf(err error) *status.Status {
	if errors.Is(err, geoobjects.ErrEmptyArea) {
		return status.New(codes.InvalidArgument, err.Error())
	}

	return status.New(codes.Internal, err.Error())
}
