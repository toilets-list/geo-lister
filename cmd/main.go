package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/joho/godotenv"
	"github.com/tmc/grpc-websocket-proxy/wsproxy"
	geoobjects "gitlab.com/toilets-list/geo-lister/internal/geo_objects"
	"gitlab.com/toilets-list/geo-lister/internal/server"
	"gitlab.com/toilets-list/geo-lister/internal/sql"
	geo_lister "gitlab.com/toilets-list/geo-lister/pkg/api_pb/gitlab.com/toilets-list/geo-lister/geo-lister"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	maxCallRecvMsgSize = 50000000
	grpcPort           = 8001
	httpPort           = 8003
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("No .env file found")
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	storage, err := sql.NewStorage(ctx, sql.ConnectionStr())
	if err != nil {
		log.Panic(err)
	}
	service := server.NewGeoService(
		geoobjects.NewGetGeoObjectHandler(storage),
		geoobjects.NewCreateGeoObjectHandler(storage),
		geoobjects.NewListGeoObjectInAreaHandler(storage),
		geoobjects.NewDeleteGeoObjectHandler(storage),
		geoobjects.NewUpdateGeoObjectHandler(storage),
	)

	err = run(ctx, service)
	if err != nil {
		log.Panic(err)
	}
}

func run(ctx context.Context, service geo_lister.GeoServiceServer) error {
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", grpcPort))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	var group errgroup.Group

	mux := runtime.NewServeMux(runtime.WithMarshalerOption(
		runtime.MIMEWildcard,
		&runtime.JSONPb{OrigName: true, EmitDefaults: true},
	))
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(maxCallRecvMsgSize)),
	}

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_recovery.UnaryServerInterceptor(),
		)),
	)
	geo_lister.RegisterGeoServiceServer(grpcServer, service)

	group.Go(func() error {
		return grpcServer.Serve(lis)
	})
	group.Go(func() error {
		return geo_lister.RegisterGeoServiceHandlerFromEndpoint(ctx, mux, ":"+strconv.Itoa(grpcPort), opts)
	})
	group.Go(func() error {
		httpServer := http.Server{
			WriteTimeout: time.Minute,
			ReadTimeout:  time.Minute,
			Addr:         ":" + strconv.Itoa(httpPort),
			Handler:      wsproxy.WebsocketProxy(mux),
		}

		return httpServer.ListenAndServe()
	})

	return group.Wait()
}
