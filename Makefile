generate:
	GOBIN=${CURDIR}/bin go install \
        github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
        github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
        github.com/golang/protobuf/protoc-gen-go
	protoc -I/usr/local/include -I. \
        -I${CURDIR}/api/vendor \
        --grpc-gateway_out=logtostderr=true:./pkg/api_pb --plugin=./bin/protoc-gen-grpc-gateway \
        --swagger_out=allow_merge=true,merge_file_name=api:. --plugin=./bin/protoc-gen-swagger \
        --go_out=plugins=grpc:./pkg/api_pb ./api/geo_lister/geo_lister.proto --plugin=./bin/protoc-gen-go

migrate:
	go install github.com/pressly/goose/v3/cmd/goose@latest
	goose -dir=migrations postgres "user=$(DB_USERNAME) password=$(DB_PASSWORD) dbname=$(DB_BASE) sslmode=disable" up

lint:
	golangci-lint run
